extern crate serde_json as json;

use {
    serde::{Deserialize, Serialize},
    std::{fs::File, io::BufReader, path::PathBuf},
    structopt::StructOpt,
    thiserror::Error,
};

/// Cools down burning notes from charts in the [Friday Night Funkin' Tricky Mod].
///
/// More specifically, this tool removes burning notes on the player's side and converts burning notes on the opponent's side to normal notes.
///
/// WARNING: untrickify WILL OVERWRITE THE OUTPUT FILE without asking you.
#[derive(StructOpt, Debug, Clone)]
struct Opt {
    /// The input chart.
    #[structopt(short, long)]
    input: PathBuf,

    /// The output chart.
    #[structopt(short, long)]
    output: PathBuf,
}

/// A chart.
#[derive(Serialize, Deserialize, Debug, Clone)]
struct Chart {
    song: Song,
}

/// A song.
#[derive(Serialize, Deserialize, Debug, Clone)]
struct Song {
    song: String,
    notes: Vec<Section>,
    bpm: i32,
    #[serde(rename = "needsVoices")]
    needs_voices: bool,
    speed: f32,
    player1: String,
    player2: String,
    #[serde(rename = "validScore")]
    valid_score: bool,
}

/// A section.
#[derive(Serialize, Deserialize, Debug, Clone)]
struct Section {
    #[serde(rename = "sectionNotes")]
    section_notes: Vec<Note>,
    #[serde(rename = "lengthInSteps")]
    length_in_steps: i32,
    #[serde(rename = "typeOfSection")]
    type_of_section: i32,
    #[serde(rename = "mustHitSection")]
    must_hit_section: bool,
    bpm: Option<i32>,
    #[serde(rename = "changeBPM")]
    change_bpm: Option<bool>,
    #[serde(rename = "altAnim")]
    alt_anim: Option<bool>,
}

/// A note.
///
/// Uses special serialization and deserialization as Friday Night Funkin' encodes notes as arrays.
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(from = "(f32, i32, f32)", into = "(f32, i32, f32)")]
struct Note {
    strum_time: f32,
    note_data: i32,
    sustain_length: f32,
}

impl From<(f32, i32, f32)> for Note {
    fn from((strum_time, note_data, sustain_length): (f32, i32, f32)) -> Self {
        Self {
            strum_time,
            note_data,
            sustain_length,
        }
    }
}

impl From<Note> for (f32, i32, f32) {
    fn from(
        Note {
            strum_time,
            note_data,
            sustain_length,
        }: Note,
    ) -> Self {
        (strum_time, note_data, sustain_length)
    }
}

#[derive(Error, Debug)]
enum Error {
    #[error("opening input file at {0}: {1}")]
    Input(PathBuf, std::io::Error),
    #[error("creating output file at {0}: {1}")]
    Output(PathBuf, std::io::Error),
    #[error("deserializing chart: {0}")]
    Deserialization(json::error::Error),
    #[error("serializing chart: {0}")]
    Serialization(json::error::Error),
}

fn run(opt: Opt) -> Result<(), Error> {
    let input_file = match File::open(&opt.input) {
        Ok(file) => file,
        Err(error) => {
            return Err(Error::Input(opt.input, error));
        }
    };

    let output_file = match File::create(&opt.output) {
        Ok(file) => file,
        Err(error) => {
            return Err(Error::Output(opt.output, error));
        }
    };

    let input_chart: Chart = match json::from_reader(BufReader::new(input_file)) {
        Ok(chart) => chart,
        Err(error) => {
            return Err(Error::Deserialization(error));
        }
    };

    let output_chart = Chart {
        song: Song {
            notes: input_chart
                .song
                .notes
                .iter()
                .map(|section| Section {
                    section_notes: section
                        .section_notes
                        .clone()
                        .into_iter()
                        .filter_map(|note: Note| {
                            if note.note_data > 7 {
                                let cooled_note_data = note.note_data - 8;
                                if !(section.must_hit_section ^ (cooled_note_data > 3)) {
                                    Some(Note {
                                        note_data: cooled_note_data,
                                        ..note
                                    })
                                } else {
                                    None
                                }
                            } else {
                                Some(note)
                            }
                        })
                        .collect(),
                    ..*section
                })
                .collect(),
            ..input_chart.song
        },
    };

    if let Err(error) = json::to_writer(output_file, &output_chart) {
        return Err(Error::Serialization(error));
    }

    Ok(())
}

fn main() {
    let opt = Opt::from_args();

    std::process::exit(if let Err(error) = run(opt) {
        eprintln!("{}", error);
        1
    } else {
        0
    })
}
