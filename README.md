# untrickify

Cools down burning notes from charts in the [Friday Night Funkin' Tricky Mod](https://gamebanana.com/mods/44334).

More specifically, this tool removes burning notes on the player's side and converts burning notes on the opponent's side to normal notes.

**WARNING**: `untrickify` **WILL OVERWRITE THE OUTPUT FILE** without asking you.

## observations

`noteData` works like this:

```
mustHitSection: false
# normal notes
<   v   ^   >
0,  1,  2,  3  : player2
4,  5,  6,  7  : player1

# burning notes
<   v   ^   >
8,  9,  10, 11 : player2
12, 13, 14, 15 : player1

mustHitSection: true
# normal notes
<   v   ^   >
0,  1,  2,  3  : player1
4,  5,  6,  7  : player2

# burning notes
<   v   ^   >
8,  9,  10, 11 : player1
12, 13, 14, 15 : player2
```

So to follow the same process this tool does, you can do the following:

```rs
if note_data > 7 {
    let cooled_note_data = note_data - 8;
    if !(must_hit_section ^ (cooled_note_data > 3)) {
        Some(cooled_note_data)
    } else {
        None
    }
} else {
    Some(note_data)
}
```

Note that this doesn't handle invalid values.
